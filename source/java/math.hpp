#pragma once

#include <type_traits>
#include <java.hpp>
#include <cmath>

#define JAVA_MATH_IMPLEMENTATION(operation) \
	template <typename... Ts> \
	inline static decltype( std::operation(std::declval<Ts>()...) ) operation(Ts&&... values) \
	{ \
		return std::operation(std::forward<Ts>(values)...); \
	} \
	template <typename... Ts> \
	inline static decltype( std::operation(std::declval<Ts>()...) ) operation(const java::lang::IntegerOf<Ts>&... values) \
	{ \
		return std::operation(std::forward<Ts>(Ts(values))...); \
	} \
	template <typename... Ts> \
	inline static decltype( std::operation(std::declval<Ts>()...) ) operation(const java::lang::FloatingOf<Ts>&... values) \
	{ \
		return std::operation(std::forward<Ts>(Ts(values))...); \
	}

class java::lang::Math
{
public:
	// Trigonometric functions
	JAVA_MATH_IMPLEMENTATION(cos)
	JAVA_MATH_IMPLEMENTATION(sin)
	JAVA_MATH_IMPLEMENTATION(tan)
	JAVA_MATH_IMPLEMENTATION(acos)
	JAVA_MATH_IMPLEMENTATION(asin)
	JAVA_MATH_IMPLEMENTATION(atan)
	JAVA_MATH_IMPLEMENTATION(atan2)

	// Hyperbolic functions
	JAVA_MATH_IMPLEMENTATION(cosh)
	JAVA_MATH_IMPLEMENTATION(sinh)
	JAVA_MATH_IMPLEMENTATION(tanh)
	JAVA_MATH_IMPLEMENTATION(acosh)
	JAVA_MATH_IMPLEMENTATION(asinh)
	JAVA_MATH_IMPLEMENTATION(atanh)

	// Exponential and logarithmic functions
	JAVA_MATH_IMPLEMENTATION(exp)
	JAVA_MATH_IMPLEMENTATION(frexp)
	JAVA_MATH_IMPLEMENTATION(ldexp)
	JAVA_MATH_IMPLEMENTATION(log)
	JAVA_MATH_IMPLEMENTATION(log10)
	JAVA_MATH_IMPLEMENTATION(modf)
	JAVA_MATH_IMPLEMENTATION(exp2)
	JAVA_MATH_IMPLEMENTATION(expm1)
	JAVA_MATH_IMPLEMENTATION(ilogb)
	JAVA_MATH_IMPLEMENTATION(log1p)
	JAVA_MATH_IMPLEMENTATION(log2)
	JAVA_MATH_IMPLEMENTATION(logb)
	JAVA_MATH_IMPLEMENTATION(scalbn)
	JAVA_MATH_IMPLEMENTATION(scalbln)

	// Power functions
	JAVA_MATH_IMPLEMENTATION(pow)
	JAVA_MATH_IMPLEMENTATION(sqrt)
	JAVA_MATH_IMPLEMENTATION(cbrt)
	JAVA_MATH_IMPLEMENTATION(hypot)

	// Error and gammga functions
	JAVA_MATH_IMPLEMENTATION(erf)
	JAVA_MATH_IMPLEMENTATION(erfc)
	JAVA_MATH_IMPLEMENTATION(tgamma)
	JAVA_MATH_IMPLEMENTATION(lgamma)

	// Rounding  and remainder functions
	JAVA_MATH_IMPLEMENTATION(ceil)
	JAVA_MATH_IMPLEMENTATION(floor)
	JAVA_MATH_IMPLEMENTATION(fmod)
	JAVA_MATH_IMPLEMENTATION(trunc)
	JAVA_MATH_IMPLEMENTATION(round)
	JAVA_MATH_IMPLEMENTATION(lround)
	JAVA_MATH_IMPLEMENTATION(llround)
	JAVA_MATH_IMPLEMENTATION(rint)
	JAVA_MATH_IMPLEMENTATION(lrint)
	JAVA_MATH_IMPLEMENTATION(llrint)
	JAVA_MATH_IMPLEMENTATION(nearbyint)
	JAVA_MATH_IMPLEMENTATION(remainder)
	JAVA_MATH_IMPLEMENTATION(remquo)

	// Floating-point manipulation functions
	JAVA_MATH_IMPLEMENTATION(copysign)
	JAVA_MATH_IMPLEMENTATION(nan)
	JAVA_MATH_IMPLEMENTATION(nextafter)
	JAVA_MATH_IMPLEMENTATION(nexttoward)

	// Minimum, Maximum, Difference functions
	JAVA_MATH_IMPLEMENTATION(fdim)
	JAVA_MATH_IMPLEMENTATION(fmax)
	JAVA_MATH_IMPLEMENTATION(fmin)

	// Other functions
	JAVA_MATH_IMPLEMENTATION(fabs)
	JAVA_MATH_IMPLEMENTATION(abs)
	JAVA_MATH_IMPLEMENTATION(fma)

	// Classification functions
	// ...

protected:
	Math() = default;
};

