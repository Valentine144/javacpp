#pragma once

#include <java.hpp>

namespace java
{

	template <std::size_t __MemSize>
	class Allocator_
	{
		byte memory[ __MemSize ];
	public:
		constexpr std::size_t memsize() { return __MemSize; }

		java::ReferenceTo<byte> begin();
		java::ReferenceTo<byte> end();
		java::ReferenceTo<byte> current = begin();

		java::Reference do_alloc(std::size_t);
		void do_free(java::Reference);

		template <typename _Tp, typename... _Args>
		java::ReferenceTo<_Tp> construct(_Args&&...);
		
		template <typename _Tp>
		void destroy(java::ReferenceTo<_Tp>);

		template <typename _Tp>
		java::ReferenceTo<_Tp> constructArray(std::size_t);

		template <typename _Tp>
		void destroyArray(java::ReferenceTo<_Tp>);
	};

	using Allocator = Allocator_<1024*1024>; // default: 1MB;

	struct Memory 
	{
		static constexpr std::size_t BASIC_SIZE = 32*1024*1024;
		static constexpr std::size_t EXT_SIZE = 8*1024*1024;

		static Allocator_<BASIC_SIZE>& basic()
		{
			static Allocator_<BASIC_SIZE> basic_ator;
			return basic_ator;
		}

		static Allocator_<EXT_SIZE>& ext()
		{
			static Allocator_<EXT_SIZE> ext_ator;
			return ext_ator;
		}
	};

}

/// include template implementations to resolve linkage
#include <java/common/java_allocator_impl.cxx>