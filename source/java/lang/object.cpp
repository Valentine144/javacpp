#include <java/lang.hpp>
#include <java/lang/object/impl.hpp>

#include <java/primitive/Integer>

#include <java/common/java_allocator.hpp>


boolean 
java::lang::Object::isNull() const
{
	return(!pimpl.get());
}

boolean 
java::lang::Object::equals(const java::lang::Object& other) const
{
	return this->getImpl().equals(other.getImpl());
}

java::lang::Object 	
java::lang::Object::clone() const
{
	return getImpl().clone();
}

long 	
java::lang::Object::hashCode() const
{
	return getImpl().hashCode();
}

java::lang::Class 	
java::lang::Object::getClass() const
{
	return getImpl().getClass();
} 

void 	
java::lang::Object::finalize()
{
	getImpl().finalize();
} 

java::lang::String 
java::lang::Object::toString() const
{
	return getClass().getName() + " @" + IntegerOf<long>(hashCode()).toHexString();
}


/**
 * C++ Implementation
 */
// java::Reference
// java::lang::Object::operator new(std::size_t n)
// {
// 	return java::Memory::basic().do_alloc(n);
// }

// void 
// java::lang::Object::operator delete(java::Reference ref)
// {
// 	java::Memory::basic().do_free(ref);
// }

java::lang::Object::Object():
	pimpl(new java::lang::Object::Impl)
{}

java::lang::Object::~Object()
{
	finalize();
}

java::lang::Object::Impl&
java::lang::Object::getImpl()
{
	return(*pimpl);
}

const java::lang::Object::Impl&
java::lang::Object::getImpl() const
{
	return(*pimpl);
}
