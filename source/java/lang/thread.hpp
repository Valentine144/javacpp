#pragma once

#include <java/lang/Object>

namespace java
{
	namespace lang
	{
		class Runnable
		{
		public:
			virtual ~Runnable() = 0;
			virtual void run() = 0;
		protected:
			Runnable() = default;
		};
	}
}

#define extends : public
#define implements , public

class java::lang::Thread 
	extends java::lang::Object
	implements Runnable
{
	java_object(java::lang, Thread)
public:
	Thread();

	template <typename Signature, typename... Arguments>
	Thread(const std::function<Signature>&, Arguments...); // std::is_same<void(Arguments...), Signature>::value

	void run() override;
	void start();
}