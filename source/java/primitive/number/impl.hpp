#pragma once

#include <java/primitive/number.hpp>
#include <java/lang/object/impl.hpp>

class java::lang::Number::Impl : public java::lang::Object::Impl
{
	java_object_impl(Number)
public:
	Impl() = default;
	virtual ~Impl() = default;
	
	virtual java::lang::String toDecimalString() const;
	virtual java::lang::String toBinaryString() const;
	virtual java::lang::String toOctalString() const;
	virtual java::lang::String toHexString() const;
	virtual int compareTo(const java::lang::Number::Impl&) const;
};