#include <java/primitive/of/impl.hpp>

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>::PrimitiveOf():
	java::lang::Number(new java::lang::PrimitiveOf<I, TCHK>::Impl)
{}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>::PrimitiveOf(const I& value):
	java::lang::Number(new java::lang::PrimitiveOf<I, TCHK>::Impl(value))
{}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>::PrimitiveOf(I&& value) noexcept:
	java::lang::Number(new java::lang::PrimitiveOf<I, TCHK>::Impl(std::move(value)))
{}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>& 
java::lang::PrimitiveOf<I, TCHK>::operator=(const I& value)
{
	getImpl() = value;
	return(*this);
}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>& 
java::lang::PrimitiveOf<I, TCHK>::operator=(I&& value) noexcept
{
	getImpl() = std::move(value);
	return(*this);
}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK> 
java::lang::PrimitiveOf<I, TCHK>::fromString(const String& inp)
{
	return java::lang::PrimitiveOf<I, TCHK>::Impl::fromString(inp);
}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>::operator reference()
{
	return getImpl().get_ref();
}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>::operator pointer()
{
	return getImpl().get_ptr();
}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>::operator const_reference() const
{
	return getImpl().get_const_ref();
}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK>::operator const_pointer() const
{
	return getImpl().get_const_ptr();
}

template <typename I, template <typename> class TCHK>
template <typename U, template <typename> class UCHK>
java::lang::PrimitiveOf<I, TCHK>::operator PrimitiveOf<U, UCHK>() const
{
	return typename java::lang::PrimitiveOf<U, UCHK>( static_cast<const U>(getImpl().get_value()) );
}

// template <typename I, template <typename> class TCHK>
// template <typename U, template <typename> class UCHK>
// PrimitiveOf<U, UCHK>
// java::lang::PrimitiveOf<I, TCHK>::cast() const
// {
// }

// /**
//  * C++ Implementation
//  */
// template <typename I, template <typename> class TCHK>
// java::lang::PrimitiveOf<I, TCHK>::PrimitiveOf(java::lang::Object::Impl* impl):
// 	java::lang::Number(impl)
// {}

// template <typename I, template <typename> class TCHK>
// java::lang::PrimitiveOf<I, TCHK>::PrimitiveOf(std::shared_ptr<java::lang::PrimitiveOf<I, TCHK>::Impl> impl):
// 	java::lang::Number(std::dynamic_pointer_cast<java::lang::Object::Impl>(impl))
// {}

// template <typename I, template <typename> class TCHK>
// java::lang::PrimitiveOf<I, TCHK>& 
// java::lang::PrimitiveOf<I, TCHK>::operator=(java::lang::PrimitiveOf<I, TCHK>::Impl* impl)
// {
// 	pimpl.reset(impl);
// 	return(*this);
// }

// template <typename I, template <typename> class TCHK>
// java::lang::PrimitiveOf<I, TCHK>& 
// java::lang::PrimitiveOf<I, TCHK>::operator=(std::shared_ptr<java::lang::PrimitiveOf<I, TCHK>::Impl> impl)
// {
// 	pimpl = std::dynamic_pointer_cast<java::lang::Object::Impl>(impl);
// 	return(*this);
// }

// template <typename I, template <typename> class TCHK>
// typename java::lang::PrimitiveOf<I, TCHK>::Impl& 
// java::lang::PrimitiveOf<I, TCHK>::getImpl()
// {
// 	return *dynamic_cast<java::lang::PrimitiveOf<I, TCHK>::Impl*>(pimpl.get());
// }

// template <typename I, template <typename> class TCHK>
// const typename java::lang::PrimitiveOf<I, TCHK>::Impl& 
// java::lang::PrimitiveOf<I, TCHK>::getImpl() const
// {
// 	return *dynamic_cast<java::lang::PrimitiveOf<I, TCHK>::Impl*>(pimpl.get());
// }
