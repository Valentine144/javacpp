#include <java/lang.hpp>
#include <java/primitive/number.hpp>
#include <java/primitive/number/impl.hpp>

java::lang::Number::Number():
	java::lang::Object(new java::lang::Number::Impl)
{}

java::lang::String 
java::lang::Number::toDecimalString() const
{
	return getImpl().toDecimalString();
}

java::lang::String 
java::lang::Number::toBinaryString() const
{
	return getImpl().toBinaryString();
}

java::lang::String 
java::lang::Number::toOctalString() const
{
	return getImpl().toOctalString();
}

java::lang::String 
java::lang::Number::toHexString() const
{
	return getImpl().toHexString();
}

int 
java::lang::Number::compareTo(const java::lang::Number& other) const
{
	return getImpl().compareTo(other.getImpl());
}