#include <java/lang.hpp>
#include <java/lang/object/impl.hpp>
#include <java/primitive/primitive_of.hpp>
#include <java/primitive/of/impl.hpp>
#include <algorithm>
#include <java/lang/Math>

// template <typename I, template <typename> class TCHK>
// typename java::lang::Object::Impl* 
// java::lang::PrimitiveOf<I, TCHK>::Impl::clone() const
// {
// 	return new java::lang::PrimitiveOf<I, TCHK>::Impl(*this);
// }

// template <typename I, template <typename> class TCHK>
// java::lang::Class 
// java::lang::PrimitiveOf<I, TCHK>::Impl::getClass() const
// {
// 	return java::lang::Class::from<java::lang::PrimitiveOf<I, TCHK>>();
// }

template <typename I, template <typename> class TCHK>
java::lang::String 
java::lang::PrimitiveOf<I, TCHK>::Impl::toDecimalString() const 
{
	return std::is_same<I, boolean>::value ? (data ? "true" : "false") : std::to_string(data);
}

template <long to_power>
struct decimalTo
{
	template <typename I>
	static java::lang::String  convert/*_integer*/(const I& number)
	{
		bool sign = number < 0;

		java::lang::String output = "";
	
		long value{java::lang::Math::abs(number)};
		while(value)
		{
			output += char('0' + value%to_power);
			value /= to_power;
		}

		if (sign) { output += '-'; }

		std::reverse(output.begin(), output.end());
		return output;
	}

	// template <typename F>
	// static java::lang::String convert_float(const F& number)
	// {
	// 	bool sign = number < 0.0;

	// 	F value{java::lang::Math::fabs(number)};
	// 	F floatpart = (value - java::lang::Math::trunc(value))*10.0;
	// 	while (floatpart - java::lang::Math::trunc(floatpart) > 0.0) { floatpart *= 10; }

	// 	return (sign ? "-" : "") + convert_integer(long(value)) + '.' + convert_integer(long(floatpart));
	// }

	// template <typename A>
	// static java::lang::String convert(const A& number)
	// {
	// 	return std::is_same<A, boolean>::value ? (number ? "1" : "0") : (std::is_integral<A>::value ? convert_integer(number) : convert_float(number));  
	// }
};

template <>
struct decimalTo<16>
{
	template <typename I>
	static java::lang::String convert/*_integer*/(const I& number)
	{
		java::lang::String output = "";
	
		long value{number};
		while(value)
		{
			using ushort_t = unsigned short;
			ushort_t digit = ushort_t(value%16);
			output += digit < 10 ? char('0' + digit) : char('A' + digit%10);
			value /= 16;
		}

		std::reverse(output.begin(), output.end());
		return output;
	}

	// template <typename F>
	// static java::lang::String convert_float(const F& number)
	// {
	// 	bool sign = number < 0.0;

	// 	F value{java::lang::Math::fabs(number)};
	// 	F floatpart = (value - java::lang::Math::trunc(value))*10.0;

	// 	while (floatpart - java::lang::Math::trunc(floatpart) > 0.0) { floatpart *= 10.0; }

	// 	return (sign ? "-" : "") + convert_integer(long(value)) + '.' + (floatpart == 0.0 ? "0000" : convert_integer(long(floatpart)));
	// }

	// template <typename A>
	// static java::lang::String convert(const A& number)
	// {
	// 	return std::is_same<A, boolean>::value ? (number ? "1" : "0") : (std::is_integral<A>::value ? convert_integer(number) : convert_float(number));  
	// }
};

template <typename I, template <typename> class TCHK>
java::lang::String 
java::lang::PrimitiveOf<I, TCHK>::Impl::toBinaryString() const 
{
	return decimalTo<2>::convert(data);
}
	
template <typename I, template <typename> class TCHK>
java::lang::String 
java::lang::PrimitiveOf<I, TCHK>::Impl::toOctalString() const 
{	
	return decimalTo<8>::convert(data);
}

template <typename I, template <typename> class TCHK>
java::lang::String 
java::lang::PrimitiveOf<I, TCHK>::Impl::toHexString() const 
{
	return decimalTo<16>::convert(data);
}

template <typename I, template <typename> class TCHK>
int 
java::lang::PrimitiveOf<I, TCHK>::Impl::compareTo(const java::lang::Number::Impl& other) const 
{
	return int(data - other.as<java::lang::PrimitiveOf<I, TCHK>::Impl>().data);
}


template <typename I, template <typename> class TCHK>
typename java::lang::PrimitiveOf<I, TCHK>::Impl& 
java::lang::PrimitiveOf<I, TCHK>::Impl::operator=(const value_type& value)
{
	data = value;
	return(*this);
}

template <typename I, template <typename> class TCHK>
typename java::lang::PrimitiveOf<I, TCHK>::Impl& 
java::lang::PrimitiveOf<I, TCHK>::Impl::operator=(value_type&& value) noexcept
{
	data = std::move(value);
	return(*this);
}

template <typename I, template <typename> class TCHK>
java::lang::PrimitiveOf<I, TCHK> 
java::lang::PrimitiveOf<I, TCHK>::Impl::fromString(const java::lang::String& inp)
{
	return java::lang::PrimitiveOf<I, TCHK>(java::detail::NumberParser<I>::from(inp));
}