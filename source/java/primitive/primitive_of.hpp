#pragma once

#include <java/primitive/number.hpp>

template <typename T, template <typename> class type_checker>
class java::lang::PrimitiveOf : public java::lang::Number
{
	static_assert(type_checker<T>::value, "type error");

	using value_type = T;
	using reference = value_type &;
	using pointer = value_type *;
	using const_reference = const value_type &;
	using const_pointer = const value_type *;

	using this_type = PrimitiveOf<value_type, type_checker>;

	java_object_from(java::lang, PrimitiveOf, java::lang::Number);
public:
	PrimitiveOf();

	PrimitiveOf(const value_type&);
	PrimitiveOf(value_type&&) noexcept;

	this_type& operator=(const value_type&);
	this_type& operator=(value_type&&) noexcept;

	~PrimitiveOf() = default;
	
	static this_type fromString(const String&);

	operator reference();
	operator pointer();
	operator const_reference() const;
	operator const_pointer() const;

	template <typename U, template <typename> class C>
	operator PrimitiveOf<U, C>() const;

	template <typename U, template <typename> class C>
	this_type& operator=(const PrimitiveOf<U, C>& o)
	{
		return this->this_type::operator=(value_type(o.PrimitiveOf<U,C>::operator const_reference()));
	}

	template <typename U, template <typename> class C>
	this_type& operator=(PrimitiveOf<U, C>&& o)
	{
		return this->this_type::operator=(value_type(o.PrimitiveOf<U,C>::operator const_reference()));
	}

	// template <typename U, template <typename> class C>
	// PrimitiveOf<U, C> cast() const;
};